<?php

namespace Code\Module;

use App;

use Code\Lib\ActivityStreams;
use Code\Web\Controller;

class Apresolver extends Controller
{
    public function init()
    {
        if (ActivityStreams::is_as_request()) {
            // Concatenate path components starting with argv(1)
            // to isolate the DID URL.
            $url = null;
            for ($index = 1; $index < argc(); $index ++) {
                if ($index != 1) {
                    $url .= '/';
                }
                $url .= argv($index);
            }
            // Extract the ed25519 key from the DID URL.
            $key = str_replace('did:ap:key:', '', $url);
            $key = rtrim($key, '/');
            $index = strpos($key, '/');
            $key = substr($key, 0, $index ?: null);

            // Find a channel on this site which has that ed25519 key.
            $query = q("select * from xchan left join channel 
                on xchan_hash = channel_hash where xchan_epubkey = '%s'",
                dbesc($key)
            );
            if (!($query && isset($query[0]['channel_id']))) {
                http_status_exit(404, 'Not found');
            }
            $mappedPath = $this->mapObject(str_replace('did:ap:key:' . $key, '', rtrim($url, '/')), $query[0]);
            $localPath = ltrim($mappedPath, '/');
            App::$cmd = $localPath;
            $controller = "\\Code\Module\\" . ucfirst(substr($localPath, 0, strpos($localPath, '/')));
            $module = new $controller;
            App::$argv = explode('/', App::$cmd);

            App::$argc = count(App::$argv);
            if ((array_key_exists('0', App::$argv)) && strlen(App::$argv[0])) {
                if (strpos(App::$argv[0],'.')) {
                    $_REQUEST['module_format'] = substr(App::$argv[0],strpos(App::$argv[0], '.') + 1);
                    App::$argv[0] =  substr(App::$argv[0], 0, strpos(App::$argv[0], '.'));
                }

                App::$module = str_replace(".", "_", App::$argv[0]);
                App::$module = str_replace("-", "_", App::$module);
                if (str_starts_with(App::$module, '_')) {
                    App::$module = substr(App::$module, 1);
                }
            }
            else {
                App::$argc = 1;
                App::$argv = ['home'];
                App::$module = 'home';
            }
            header('Link: ' . '<'  . $url . '>; rel="alternate"', false);
            $module->init();

        }
    }

    protected function mapObject($path, $channel)
    {
        // lookup abstract paths
        $systemPaths = [
            '' => '/channel/' . $channel['channel_address'],
            '/inbox' => '/inbox/' . $channel['channel_address'],
            '/outbox' => '/outbox/' . $channel['channel_address'],
            '/followers' =>	'/followers/' . $channel['channel_address'],
            '/following' =>	'/following/' . $channel['channel_address'],
        ];

        foreach ($systemPaths as $index => $localPath) {
            if ($path === $index) {
                return $localPath;
            }
        }
        return $path;
    }

}
