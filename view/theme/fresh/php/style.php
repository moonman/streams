<?php

use Code\Lib\Channel;

if (!App::$install) {

    // Get the UID of the channel owner
    $uid = Channel::get_theme_uid();

    if ($uid) {
        load_pconfig($uid, 'fresh');
    }

    // Load the owners pconfig
    $banner_color = get_pconfig($uid, 'fresh', 'banner_color');
//    $narrow_navbar = get_pconfig($uid, 'fresh', 'narrow_navbar');
    $link_color = get_pconfig($uid, 'fresh', 'link_color');
    $link_hover = get_pconfig($uid, 'fresh', 'link_hover');
    $schema = get_pconfig($uid, 'fresh', 'schema');
    $bgcolor = get_pconfig($uid, 'fresh', 'background_color');
    $overlaybg = get_pconfig($uid, 'fresh', 'overlay_background_color');
    $subtle_shade = get_pconfig($uid, 'fresh', 'subtle_shade');
    $mid_shade = get_pconfig($uid, 'fresh', 'mid_shade');
    $contrast_shade = get_pconfig($uid, 'fresh', 'contrast_shade');
    $background_image = get_pconfig($uid, 'fresh', 'background_image');
    $font_size = get_pconfig($uid, 'fresh', 'font_size');
    $font_color = get_pconfig($uid, 'fresh', 'font_color');
    $font_contrast_color = get_pconfig($uid, 'fresh', 'font_contrast_color');
    $primary = get_pconfig($uid, 'fresh', 'primary');
    $secondary = get_pconfig($uid, 'fresh', 'secondary');
    $action = get_pconfig($uid, 'fresh', 'action');
    $success = get_pconfig($uid, 'fresh', 'success');
    $warning = get_pconfig($uid, 'fresh', 'warning');
    $danger = get_pconfig($uid, 'fresh', 'danger');
    $border = get_pconfig($uid, 'fresh', 'border');
    $radius = get_pconfig($uid, 'fresh', 'radius');
    $shadow = get_pconfig($uid, 'fresh', 'photo_shadow');
    $converse_width = get_pconfig($uid, 'fresh', 'converse_width');
    $top_photo = get_pconfig($uid, 'fresh', 'top_photo');
    $reply_photo = get_pconfig($uid, 'fresh', 'reply_photo');
}

// Now load the scheme.  If a value is changed above, we'll keep the settings
// If not, we'll keep those defined by the schema
// Setting $schema to '' wasn't working for some reason, so we'll check it's
// not --- like the mobile theme does instead.

// Allow layouts to over-ride the schema - used as a filename component so sanitize.

$schema = str_replace(['/', '.'], ['', ''], ((isset($_REQUEST['schema']) && $_REQUEST['schema']) ? $_REQUEST['schema'] : EMPTY_STR));


if (($schema) && ($schema != '---')) {

    // Check it exists, because this setting gets distributed to clones
    if (file_exists('view/theme/fresh/schema/' . $schema . '.php')) {
        $schemefile = 'view/theme/fresh/schema/' . $schema . '.php';
        require_once($schemefile);
    }

    if (file_exists('view/theme/fresh/schema/' . $schema . '.css')) {
        $schemecss = file_get_contents('view/theme/fresh/schema/' . $schema . '.css');
    }

}

// Allow admins to set a default schema for the hub.
// default.php and default.css MUST be symlinks to existing schema files in view/theme/fresh/schema
if ((!$schema) || ($schema == '---')) {

    if (file_exists('view/theme/fresh/schema/default.php')) {
        $schemefile = 'view/theme/fresh/schema/default.php';
        require_once($schemefile);
    }

    if (file_exists('view/theme/fresh/schema/default.css')) {
        $schemecss = file_get_contents('view/theme/fresh/schema/default.css');
    }

}

//Set some defaults - we have to do this after pulling owner settings, and we have to check for each setting
//individually.  If we don't, we'll have problems if a user has set one, but not all options.
if (!(isset($link_color) && $link_color))
    $link_color = '#144c6e';
if (!(isset($link_hover) && $link_hover))
    $link_hover = '#059daf';
if (!(isset($banner_color) && $banner_color))
    $banner_color = '#eee';
if (!(isset($bgcolor) && $bgcolor))
    $bgcolor = '#eaeaea';
if (!(isset($overlaybg) && $overlaybg))
    $overlaybg = '#fff';
if (!(isset($subtle_shade) && $subtle_shade))
    $subtle_shade = '#eee';
if (!(isset($mid_shade) && $mid_shade))
    $mid_shade = '#888';
if (!(isset($contrast_shade) && $contrast_shade))
    $contrast_shade = '#222';
if (!(isset($background_image) && $background_image))
    $background_image = '';
if (!(isset($item_opacity) && $item_opacity))
    $item_opacity = '1';
if (!(isset($font_size) && $font_size))
    $font_size = '1rem';
if (!(isset($font_color) && $font_color))
    $font_color = '#222';
if (!(isset($font_contrast_color) && $font_contrast_color))
    $font_contrast_color = '#fff';
if (!(isset($primary) && $primary))
    $primary = '#144c6e';
if (!(isset($secondary) && $secondary))
    $secondary = '#059daf';
if (!(isset($action) && $action))
    $action = '#37cfa9';
if (!(isset($success) && $success))
    $success = '#02b160';
if (!(isset($warning) && $warning))
    $warning = '#d5c029';
if (!(isset($danger) && $danger))
    $danger = '#d13f43';
if (!(isset($border) && $border))
    $border = '1px solid #ccc';
if (!(isset($radius) && $radius))
    $radius = '3px';
if (!(isset($shadow) && $shadow))
    $shadow = '0 1px 4px #aaa';
if (!(isset($converse_width) && $converse_width))
    $converse_width = '790';
if (!(isset($top_photo) && $top_photo))
    $top_photo = '2.3rem';
if (!(isset($reply_photo) && $reply_photo))
    $reply_photo = '2.3rem';

// Apply the settings
if (file_exists('view/theme/fresh/css/style.css')) {

    $x = file_get_contents('view/theme/fresh/css/style.css');

//    if ($narrow_navbar && file_exists('view/theme/fresh/css/narrow_navbar.css')) {
//       $x .= file_get_contents('view/theme/fresh/css/narrow_navbar.css');
//    }

    if (isset($schemecss)) {
        $x .= $schemecss;
    }

    $aside_width = 288;

    // left aside and right aside are 285px + converse width
    $main_width = (($aside_width * 2) + intval($converse_width));

    // prevent main_width smaller than 768px
    $main_width = (($main_width < 768) ? 768 : $main_width);

    $options = array(
        '$link_color' => $link_color,
        '$link_hover' => $link_hover,
        '$banner_color' => $banner_color,
        '$bgcolor' => $bgcolor,
        '$overlaybg' => $overlaybg,
        '$subtle_shade' => $subtle_shade,
        '$mid_shade' => $mid_shade,
        '$contrast_shade' => $contrast_shade,
        '$background_image' => $background_image,
        '$font_size' => $font_size,
        '$font_color' => $font_color,
        '$font_contrast_color' => $font_contrast_color,
        '$primary' => $primary,
        '$secondary' => $secondary,
        '$action' => $action,
        '$success' => $success,
        '$warning' => $warning,
        '$danger' => $danger,
        '$border' => $border,
        '$radius' => $radius,
        '$shadow' => $shadow,
        '$converse_width' => $converse_width,
        '$top_photo' => $top_photo,
        '$reply_photo' => $reply_photo,
        '$main_width' => $main_width,
        '$aside_width' => $aside_width
    );

    echo str_replace(array_keys($options), array_values($options), $x);

}

// Set the schema to the default schema in derived themes. See the documentation for creating derived themes how to override this. 

if (local_channel() && App::$channel && App::$channel['channel_theme'] != 'fresh')
    set_pconfig(local_channel(), 'fresh', 'schema', '---');
